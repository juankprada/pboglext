﻿; *****************************************************************
; glfw3.static.pbi
; *****************************************************************

CompilerIf (#PB_Compiler_OS = #PB_OS_MacOS)

 ; Taken by iterating through link errors and the Apple docs :-)
 ImportC "/System/Library/Frameworks/Quartz.framework/Quartz" : EndImport
 ImportC "/System/Library/Frameworks/IOKit.framework/IOKit" : EndImport
 ImportC "/System/Library/Frameworks/Foundation.framework/Foundation" : EndImport
 ImportC "/System/Library/Frameworks/AppKit.framework/AppKit" : EndImport

CompilerIf (#PB_Compiler_Processor = #PB_Processor_x86)
 ImportC #GLFW_LIBS_PATH$ + "/" + "glfw3.osx.x86.a"
CompilerElse   
 ImportC #GLFW_LIBS_PATH$ + "/" + "glfw3.osx.x64.a" 
CompilerEndIf

CompilerEndIf

CompilerIf (#PB_Compiler_OS = #PB_OS_Linux)

; Taken from the dependencies list created by CMake 
; /usr/lib/x86_64-linux-gnu/libGLU.so
; /usr/lib/x86_64-linux-gnu/libX11.so
; /usr/lib/x86_64-linux-gnu/librt.so
; /usr/lib/x86_64-linux-gnu/libXrandr.so
; /usr/lib/x86_64-linux-gnu/libXi.so
; /usr/lib/x86_64-linux-gnu/libXxf86vm.so
; /usr/lib/x86_64-linux-gnu/libm.so
; /usr/lib/x86_64-linux-gnu/libGL.so

 
CompilerIf (#PB_Compiler_Processor = #PB_Processor_x86)
 ImportC #GLFW_LIBS_PATH$ + "/" + "glfw3.lin.x86.a" : EndImport
CompilerElse   
 ImportC #GLFW_LIBS_PATH$ + "/" + "glfw3.lin.x64.a" : EndImport
CompilerEndIf

 
 ImportC "-lX11" : EndImport 
 ImportC "-lrt" : EndImport  
 ImportC "-lXrandr" : EndImport 
 ImportC "-lXi" : EndImport
 ImportC "-lXxf86vm" : EndImport
 ImportC "-lm" : EndImport
 ImportC "-lpthread" : EndImport
 
CompilerIf (#PB_Compiler_Unicode = 1)
 Import "" 
  pb_wcslen
 EndImport
CompilerEndIf
 
CompilerIf (#PB_Compiler_Processor = #PB_Processor_x86)
 ImportC #GLFW_LIBS_PATH$ + "/" + "glfw3.lin.x86.a"
CompilerElse   
 ImportC #GLFW_LIBS_PATH$ + "/" + "glfw3.lin.x64.a" 
CompilerEndIf

CompilerEndIf

CompilerIf (#PB_Compiler_OS = #PB_OS_Windows)

CompilerIf (#PB_Compiler_Processor = #PB_Processor_x86)
 ImportC #GLFW_LIBS_PATH$ + "/" + "glfw3.win.x86.lib"
CompilerElse   
 ImportC #GLFW_LIBS_PATH$ + "/" + "glfw3.win.x64.lib"
CompilerEndIf

CompilerEndIf

  glfwInit.i()
  glfwTerminate()
  glfwGetVersion(*major, *minor, *rev)
  glfwGetVersionString.i()
  glfwSetErrorCallback.i(*cbfun)
  glfwGetMonitors.i(*count)
  glfwGetPrimaryMonitor.i()
  glfwGetMonitorPos(monitor, *xpos, *ypos)
  glfwGetMonitorPhysicalSize(monitor, *width, *height)
  glfwGetMonitorName.i(monitor)
  glfwSetMonitorCallback.i(*cbfun)
  glfwGetVideoModes.i(monitor, *count)
  glfwGetVideoMode.i(monitor)
  glfwSetGamma(monitor, gamma.f)
  glfwGetGammaRamp.i(monitor)
  glfwSetGammaRamp(monitor, *ramp)
  glfwDefaultWindowHints(void)
  glfwWindowHint(target, hint)
  glfwCreateWindow.i(width, height, title.p-utf8, monitor, *share)
  glfwDestroyWindow(window)
  glfwWindowShouldClose.i(window)
  glfwSetWindowShouldClose(window, value)
  glfwSetWindowTitle(window, title.p-utf8)
  glfwGetWindowPos(window, *xpos, *ypos)
  glfwSetWindowPos(window, xpos, ypos)
  glfwGetWindowSize(window, *width, *height)
  glfwSetWindowSize(window, width, height)
  glfwGetFramebufferSize(window, *width, *height)
  glfwIconifyWindow(window)
  glfwRestoreWindow(window)
  glfwShowWindow(window)
  glfwHideWindow(window)
  glfwGetWindowMonitor.i(window)
  glfwGetWindowAttrib.i(window, attrib)
  glfwSetWindowUserPointer(window, *pointer)
  glfwGetWindowUserPointer.i(window)
  glfwSetWindowPosCallback.i(window, *cbfun)
  glfwSetWindowSizeCallback.i(window, *cbfun)
  glfwSetWindowCloseCallback.i(window, *cbfun)
  glfwSetWindowRefreshCallback.i(window, *cbfun)
  glfwSetWindowFocusCallback.i(window, *cbfun)
  glfwSetWindowIconifyCallback.i(window, *cbfun)
  glfwSetFramebufferSizeCallback.i(window, *cbfun)
  glfwPollEvents()
  glfwWaitEvents()
  glfwGetInputMode.i(window, mode)
  glfwSetInputMode(window, mode, value)
  glfwGetKey.i(window, key)
  glfwGetMouseButton.i(window, button)
  glfwGetCursorPos(window, *xpos, *ypos)
  glfwSetCursorPos(window, *xpos, *ypos)
  glfwSetKeyCallback.i(window, *cbfun)
  glfwSetCharCallback.i(window, *cbfun)
  glfwSetMouseButtonCallback.i(window, *cbfun)
  glfwSetCursorPosCallback.i(window, *cbfun)
  glfwSetCursorEnterCallback.i(window, *cbfun)
  glfwSetScrollCallback.i(window, *cbfun)
  glfwJoystickPresent.i(joy)
  glfwGetJoystickAxes.i(joy, *count)
  glfwGetJoystickButtons.i(joy, *count)
  glfwGetJoystickName.i(joy)
  glfwSetClipboardString(window, string.p-utf8)
  glfwGetClipboardString.i(window)
  glfwGetTime.d()
  glfwSetTime(time.d)
  glfwMakeContextCurrent(window)
  glfwGetCurrentContext.i()
  glfwSwapBuffers(window)
  glfwSwapInterval(interval)
  glfwExtensionSupported.i(extension.p-utf8)
  glfwGetProcAddress.i(procname.p-utf8)
  
CompilerIf (#PB_Compiler_OS = #PB_OS_Windows)
  ; win32
  glfwGetWin32Window.i(window)
  glfwGetWGLContext.i(window)
CompilerEndIf

CompilerIf (#PB_Compiler_OS = #PB_OS_MacOS)
  ; cocoa
  glfwGetCocoaWindow.i(window)
  ; nsgl
  glfwGetNSGLContext.i(window)
CompilerEndIf

CompilerIf (#PB_Compiler_OS = #PB_OS_Linux)
  ; x11
  glfwGetX11Display.i()
  glfwGetX11Window.i(window)
  ; glx
  glfwGetGLXContext.i(window)
CompilerEndIf
  
EndImport
; IDE Options = PureBasic 5.31 (Linux - x64)
; CursorPosition = 40
; FirstLine = 13
; Folding = +l
; EnableUnicode
; EnableXP
; EnableUser
; CPU = 1
; DisableDebugger
; EnablePurifier
; EnableBuildCount = 0