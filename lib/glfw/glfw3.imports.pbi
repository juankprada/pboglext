﻿; *****************************************************************
; glfw3.imports.pbi
; *****************************************************************

#GLFW_WRAPPER_PATH$ = "lib/glfw/"
#GLFW_LIBS_PATH$ = "libs/"

IncludeFile "glfw3.declares.pbi"
IncludeFile "glfw3.static.pbi"

; IDE Options = PureBasic 5.31 (Windows - x64)
; CursorPosition = 9
; EnableUnicode
; EnableXP
; EnableUser
; CPU = 1
; EnablePurifier
; EnableBuildCount = 0