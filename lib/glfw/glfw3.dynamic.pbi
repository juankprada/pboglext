﻿; *****************************************************************
; glfw3.dynamic.pbi
; *****************************************************************

; Initialization, termination and version querying
PrototypeC.i    glfwInit()
PrototypeC      glfwTerminate()
PrototypeC      glfwGetVersion(*major, *minor, *rev)
PrototypeC.i    glfwGetVersionString()

; Error handling
PrototypeC.i    glfwSetErrorCallback(*cbfun)

; Monitors stuff
PrototypeC.i    glfwGetMonitors(*count)
PrototypeC.i    glfwGetPrimaryMonitor()
PrototypeC      glfwGetMonitorPos(monitor, *xpos, *ypos)
PrototypeC      glfwGetMonitorPhysicalSize(monitor, *width, *height)
PrototypeC.i    glfwGetMonitorName(monitor)
PrototypeC.i    glfwSetMonitorCallback(*cbfun)

PrototypeC.i    glfwGetVideoModes(monitor, *count)
PrototypeC.i    glfwGetVideoMode(monitor)
PrototypeC      glfwSetGamma(monitor, gamma.f)
PrototypeC.i    glfwGetGammaRamp(monitor)
PrototypeC      glfwSetGammaRamp(monitor, *ramp)
PrototypeC      glfwDefaultWindowHints(void)
PrototypeC      glfwWindowHint(target, hint)
PrototypeC.i    glfwCreateWindow(width, height, title.p-utf8, monitor, *share)
PrototypeC      glfwDestroyWindow(window)
PrototypeC.i    glfwWindowShouldClose(window)
PrototypeC      glfwSetWindowShouldClose(window, value)
PrototypeC      glfwSetWindowTitle(window, title.p-utf8)
PrototypeC      glfwGetWindowPos(window, *xpos, *ypos)
PrototypeC      glfwSetWindowPos(window, xpos, ypos)
PrototypeC      glfwGetWindowSize(window, *width, *height)
PrototypeC      glfwSetWindowSize(window, width, height)
PrototypeC      glfwGetFramebufferSize(window, *width, *height)
PrototypeC      glfwIconifyWindow(window)
PrototypeC      glfwRestoreWindow(window)
PrototypeC      glfwShowWindow(window)
PrototypeC      glfwHideWindow(window)
PrototypeC.i    glfwGetWindowMonitor(window)
PrototypeC.i    glfwGetWindowAttrib(window, attrib)
PrototypeC      glfwSetWindowUserPointer(window, *pointer)
PrototypeC.i    glfwGetWindowUserPointer(window)
PrototypeC.i    glfwSetWindowPosCallback(window, *cbfun)
PrototypeC.i    glfwSetWindowSizeCallback(window, *cbfun)
PrototypeC.i    glfwSetWindowCloseCallback(window, *cbfun)
PrototypeC.i    glfwSetWindowRefreshCallback(window, *cbfun)
PrototypeC.i    glfwSetWindowFocusCallback(window, *cbfun)
PrototypeC.i    glfwSetWindowIconifyCallback(window, *cbfun)
PrototypeC.i    glfwSetFramebufferSizeCallback(window, *cbfun)
PrototypeC      glfwPollEvents()
PrototypeC      glfwWaitEvents()
PrototypeC.i    glfwGetInputMode(window, mode)
PrototypeC      glfwSetInputMode(window, mode, value)
PrototypeC.i    glfwGetKey(window, key)
PrototypeC.i    glfwGetMouseButton(window, button)
PrototypeC      glfwGetCursorPos(window, *xpos, *ypos)
PrototypeC      glfwSetCursorPos(window, *xpos, *ypos)
PrototypeC.i    glfwSetKeyCallback(window, *cbfun)
PrototypeC.i    glfwSetCharCallback(window, *cbfun)
PrototypeC.i    glfwSetMouseButtonCallback(window, *cbfun)
PrototypeC.i    glfwSetCursorPosCallback(window, *cbfun)
PrototypeC.i    glfwSetCursorEnterCallback(window, *cbfun)
PrototypeC.i    glfwSetScrollCallback(window, *cbfun)
PrototypeC.i    glfwJoystickPresent(joy)
PrototypeC.i    glfwGetJoystickAxes(joy, *count)
PrototypeC.i    glfwGetJoystickButtons(joy, *count)
PrototypeC.i    glfwGetJoystickName(joy)
PrototypeC      glfwSetClipboardString(window, string.p-utf8)
PrototypeC.i    glfwGetClipboardString(window)
PrototypeC.d    glfwGetTime()
PrototypeC      glfwSetTime(time.d)
PrototypeC      glfwMakeContextCurrent(window)
PrototypeC.i    glfwGetCurrentContext()
PrototypeC      glfwSwapBuffers(window)
PrototypeC      glfwSwapInterval(interval)
PrototypeC.i    glfwExtensionSupported(extension.p-utf8)
PrototypeC.i    glfwGetProcAddress(procname.p-utf8)

; Native extra stuff

; win32
PrototypeC.i    glfwGetWin32Window(window)
PrototypeC.i    glfwGetWGLContext(window)
; cocoa
PrototypeC.i    glfwGetCocoaWindow(window)
; nsgl
PrototypeC.i    glfwGetNSGLContext(window)
; x11
PrototypeC.i    glfwGetX11Display()
PrototypeC.i    glfwGetX11Window(window)
; glx
PrototypeC.i    glfwGetGLXContext(window)


Procedure.i glfwBindLibrary (dir$) 
 ; dir$ : must be the directory where the appropriate glfw library reside.
 
 ; The actual name of the library is automatically resolved based on the platform and CPU "bitness".
 ; If successful returns the handle of the open library, else 0.
 
 ; Example:
 
 ; glfwBindLibrary (".") ; Try to open the lib in the same dir of the main executable.
 ; glfwBindLibrary ("./glfw") ; Try to open the lib in a dir "glfw" just below the the main executable.
 
 Protected glfw_lib_name$, glfw_lib_path$
 Protected ExePath$ 
 Protected RetCode, hglfw
 
 glfw_lib_name$ = "glfw3"

CompilerSelect #PB_Compiler_OS
 CompilerCase #PB_OS_Windows
  glfw_lib_name$ + ".win"  
 CompilerCase #PB_OS_Linux
  glfw_lib_name$ + ".lin"  
 CompilerCase #PB_OS_MacOS
  glfw_lib_name$ + ".osx"  
CompilerEndSelect
 
CompilerIf (#PB_Compiler_Processor = #PB_Processor_x86)
 glfw_lib_name$ + ".x86"
CompilerElse   
 glfw_lib_name$ + ".x64"
CompilerEndIf

CompilerSelect #PB_Compiler_OS
 CompilerCase #PB_OS_Windows
  glfw_lib_name$ + ".dll"  
 CompilerCase #PB_OS_Linux
  glfw_lib_name$ + ".so"  
 CompilerCase #PB_OS_MacOS
  glfw_lib_name$ + ".dylib"  
CompilerEndSelect

 glfw_lib_path$ = dir$ + "/" + glfw_lib_name$ 

 ; change current directory to the one where this executable resides
 
 ExePath$ = GetPathPart(ProgramFilename())
 
CompilerIf (#PB_Compiler_OS = #PB_OS_MacOS)
 ; exit from the OSX app bundle climbing up to the main executable .app directory
 RetCode = SetCurrentDirectory(ExePath$ + "../../../")
CompilerElse
 ; for Windows and Linux is just as usual
 RetCode = SetCurrentDirectory(ExePath$)
CompilerEndIf

 ; check if change dir was successful
 If RetCode = 0 : ProcedureReturn 0 : EndIf 

 Debug "[DEBUG] I'm about to open " + glfw_lib_path$
  
 hglfw = OpenLibrary(#PB_Any, glfw_lib_path$)
 
 If hglfw
    Global glfwInit.glfwInit = GetFunction(hglfw, "glfwInit")
    Global glfwTerminate.glfwTerminate = GetFunction(hglfw, "glfwTerminate")
    Global glfwGetVersion.glfwGetVersion = GetFunction(hglfw, "glfwGetVersion")  
    Global glfwGetVersionString.glfwGetVersionString = GetFunction(hglfw, "glfwGetVersionString")
    Global glfwSetErrorCallback.glfwSetErrorCallback = GetFunction(hglfw, "glfwSetErrorCallback") 
    Global glfwGetMonitors.glfwGetMonitors = GetFunction(hglfw, "glfwGetMonitors")
    Global glfwGetPrimaryMonitor.glfwGetPrimaryMonitor = GetFunction(hglfw, "glfwGetPrimaryMonitor")
    Global glfwGetMonitorPos.glfwGetMonitorPos = GetFunction(hglfw, "glfwGetMonitorPos")
    Global glfwGetMonitorPhysicalSize.glfwGetMonitorPhysicalSize = GetFunction(hglfw, "glfwGetMonitorPhysicalSize")
    Global glfwGetMonitorName.glfwGetMonitorName = GetFunction(hglfw, "glfwGetMonitorName")
    Global glfwSetMonitorCallback.glfwSetMonitorCallback = GetFunction(hglfw, "glfwSetMonitorCallback")
    Global glfwGetVideoModes.glfwGetVideoModes = GetFunction(hglfw, "glfwGetVideoModes")
    Global glfwGetVideoMode.glfwGetVideoMode = GetFunction(hglfw, "glfwGetVideoMode")
    Global glfwSetGamma.glfwSetGamma = GetFunction(hglfw, "glfwSetGamma")
    Global glfwGetGammaRamp.glfwGetGammaRamp = GetFunction(hglfw, "glfwGetGammaRamp")
    Global glfwSetGammaRamp.glfwSetGammaRamp = GetFunction(hglfw, "glfwSetGammaRamp")
    Global glfwDefaultWindowHints.glfwDefaultWindowHints = GetFunction(hglfw, "glfwDefaultWindowHints")
    Global glfwWindowHint.glfwWindowHint = GetFunction(hglfw, "glfwWindowHint")
    Global glfwCreateWindow.glfwCreateWindow = GetFunction(hglfw, "glfwCreateWindow")
    Global glfwDestroyWindow.glfwDestroyWindow = GetFunction(hglfw, "glfwDestroyWindow")
    Global glfwWindowShouldClose.glfwWindowShouldClose = GetFunction(hglfw, "glfwWindowShouldClose")
    Global glfwSetWindowShouldClose.glfwSetWindowShouldClose = GetFunction(hglfw, "glfwSetWindowShouldClose")
    Global glfwSetWindowTitle.glfwSetWindowTitle = GetFunction(hglfw, "glfwSetWindowTitle")
    Global glfwGetWindowPos.glfwGetWindowPos = GetFunction(hglfw, "glfwGetWindowPos")
    Global glfwSetWindowPos.glfwSetWindowPos = GetFunction(hglfw, "glfwSetWindowPos")
    Global glfwGetWindowSize.glfwGetWindowSize = GetFunction(hglfw, "glfwGetWindowSize")
    Global glfwSetWindowSize.glfwSetWindowSize = GetFunction(hglfw, "glfwSetWindowSize")
    Global glfwGetFramebufferSize.glfwGetFramebufferSize = GetFunction(hglfw, "glfwGetFramebufferSize")
    Global glfwIconifyWindow.glfwIconifyWindow = GetFunction(hglfw, "glfwIconifyWindow")
    Global glfwRestoreWindow.glfwRestoreWindow = GetFunction(hglfw, "glfwRestoreWindow")
    Global glfwShowWindow.glfwShowWindow = GetFunction(hglfw, "glfwShowWindow")
    Global glfwHideWindow.glfwHideWindow = GetFunction(hglfw, "glfwHideWindow")
    Global glfwGetWindowMonitor.glfwGetWindowMonitor = GetFunction(hglfw, "glfwGetWindowMonitor")
    Global glfwGetWindowAttrib.glfwGetWindowAttrib = GetFunction(hglfw, "glfwGetWindowAttrib")
    Global glfwSetWindowUserPointer.glfwSetWindowUserPointer = GetFunction(hglfw, "glfwSetWindowUserPointer")
    Global glfwGetWindowUserPointer.glfwGetWindowUserPointer = GetFunction(hglfw, "glfwGetWindowUserPointer")
    Global glfwSetWindowPosCallback.glfwSetWindowPosCallback = GetFunction(hglfw, "glfwSetWindowPosCallback")
    Global glfwSetWindowSizeCallback.glfwSetWindowSizeCallback = GetFunction(hglfw, "glfwSetWindowSizeCallback")
    Global glfwSetWindowCloseCallback.glfwSetWindowCloseCallback = GetFunction(hglfw, "glfwSetWindowCloseCallback")
    Global glfwSetWindowRefreshCallback.glfwSetWindowRefreshCallback = GetFunction(hglfw, "glfwSetWindowRefreshCallback")
    Global glfwSetWindowFocusCallback.glfwSetWindowFocusCallback = GetFunction(hglfw, "glfwSetWindowFocusCallback")
    Global glfwSetWindowIconifyCallback.glfwSetWindowIconifyCallback = GetFunction(hglfw, "glfwSetWindowIconifyCallback")
    Global glfwSetFramebufferSizeCallback.glfwSetFramebufferSizeCallback = GetFunction(hglfw, "glfwSetFramebufferSizeCallback")
    Global glfwPollEvents.glfwPollEvents = GetFunction(hglfw, "glfwPollEvents")
    Global glfwWaitEvents.glfwWaitEvents = GetFunction(hglfw, "glfwWaitEvents")
    Global glfwGetInputMode.glfwGetInputMode = GetFunction(hglfw, "glfwGetInputMode")
    Global glfwSetInputMode.glfwSetInputMode = GetFunction(hglfw, "glfwSetInputMode")
    Global glfwGetKey.glfwGetKey = GetFunction(hglfw, "glfwGetKey")
    Global glfwGetMouseButton.glfwGetMouseButton = GetFunction(hglfw, "glfwGetMouseButton")
    Global glfwGetCursorPos.glfwGetCursorPos = GetFunction(hglfw, "glfwGetCursorPos")
    Global glfwSetCursorPos.glfwSetCursorPos = GetFunction(hglfw, "glfwSetCursorPos")
    Global glfwSetKeyCallback.glfwSetKeyCallback = GetFunction(hglfw, "glfwSetKeyCallback")
    Global glfwSetCharCallback.glfwSetCharCallback = GetFunction(hglfw, "glfwSetCharCallback")
    Global glfwSetMouseButtonCallback.glfwSetMouseButtonCallback = GetFunction(hglfw, "glfwSetMouseButtonCallback")
    Global glfwSetCursorPosCallback.glfwSetCursorPosCallback = GetFunction(hglfw, "glfwSetCursorPosCallback")
    Global glfwSetCursorEnterCallback.glfwSetCursorEnterCallback = GetFunction(hglfw, "glfwSetCursorEnterCallback")
    Global glfwSetScrollCallback.glfwSetScrollCallback = GetFunction(hglfw, "glfwSetScrollCallback")
    Global glfwJoystickPresent.glfwJoystickPresent = GetFunction(hglfw, "glfwJoystickPresent")
    Global glfwGetJoystickAxes.glfwGetJoystickAxes = GetFunction(hglfw, "glfwGetJoystickAxes")
    Global glfwGetJoystickButtons.glfwGetJoystickButtons = GetFunction(hglfw, "glfwGetJoystickButtons")
    Global glfwGetJoystickName.glfwGetJoystickName = GetFunction(hglfw, "glfwGetJoystickName")
    Global glfwSetClipboardString.glfwSetClipboardString = GetFunction(hglfw, "glfwSetClipboardString")
    Global glfwGetClipboardString.glfwGetClipboardString = GetFunction(hglfw, "glfwGetClipboardString")
    Global glfwGetTime.glfwGetTime = GetFunction(hglfw, "glfwGetTime")
    Global glfwSetTime.glfwSetTime = GetFunction(hglfw, "glfwSetTime")
    Global glfwMakeContextCurrent.glfwMakeContextCurrent = GetFunction(hglfw, "glfwMakeContextCurrent")
    Global glfwGetCurrentContext.glfwGetCurrentContext = GetFunction(hglfw, "glfwGetCurrentContext")
    Global glfwSwapBuffers.glfwSwapBuffers = GetFunction(hglfw, "glfwSwapBuffers")
    Global glfwSwapInterval.glfwSwapInterval = GetFunction(hglfw, "glfwSwapInterval")
    Global glfwExtensionSupported.glfwExtensionSupported = GetFunction(hglfw, "glfwExtensionSupported")
    Global glfwGetProcAddress.glfwGetProcAddress = GetFunction(hglfw, "glfwGetProcAddress")
    
CompilerSelect #PB_Compiler_OS
 CompilerCase #PB_OS_Windows
    ; Win32
    Global glfwGetWin32Window.glfwGetWin32Window = GetFunction(hglfw, "glfwGetWin32Window")
    Global glfwGetWGLContext.glfwGetWGLContext = GetFunction(hglfw, "glfwGetWGLContext")  
 CompilerCase #PB_OS_Linux
    ; X11
    Global glfwGetX11Display.glfwGetX11Display = GetFunction(hglfw, "glfwGetX11Display")
    Global glfwGetX11Window.glfwGetX11Window = GetFunction(hglfw, "glfwGetX11Window")
    ; GLX
    Global glfwGetGLXContext.glfwGetGLXContext = GetFunction(hglfw, "glfwGetGLXContext")  
 CompilerCase #PB_OS_MacOS
    ; Cocoa
    Global glfwGetCocoaWindow.glfwGetCocoaWindow = GetFunction(hglfw, "glfwGetCocoaWindow")
    ; NSGL
    Global glfwGetNSGLContext.glfwGetNSGLContext = GetFunction(hglfw, "glfwGetNSGLContext") 
CompilerEndSelect
    ProcedureReturn hglfw
 EndIf
 
 ProcedureReturn 0
EndProcedure


; IDE Options = PureBasic 5.21 LTS (Windows - x64)
; Folding = -
; EnableUnicode
; EnableXP
; EnableUser
; CPU = 1
; EnablePurifier
; EnableBuildCount = 0