﻿DeclareModule GLEW
	
	#GLEW_LIBS_PATH$ = "libs/"
		
	Declare glewInit()
	
	CompilerIf (#PB_Compiler_OS = #PB_OS_Linux)
	
		CompilerIf (#PB_Compiler_Processor = #PB_Processor_x64)
			Import #GLEW_LIBS_PATH$ + "libGLEW-x64.a"
		CompilerElse
			Import #GLEW_LIBS_PATH$ + "libGLEW-x86.a"
		CompilerEndIf
			
  
	CompilerElseIf (#PB_Compiler_OS = #PB_OS_Windows)
	
		Import #PB_Compiler_Home+"PureLibraries/Windows/Libraries/opengl32.lib" : EndImport
		
		CompilerIf (#PB_Compiler_Processor = #PB_Processor_x64)
			Import #GLEW_LIBS_PATH$ + "libGLEW-x64.lib"	
		CompilerElse
			Import #GLEW_LIBS_PATH$ + "libGLEW-x86.lib"	
		CompilerEndIf		
	
	CompilerElseIf (#PB_Compiler_OS = #PB_OS_MacOS)
	
	
	CompilerEndIf

		libGlewInit() As "glewInit"
		glewGetString.i(name.i)
		glewGetErrorString.i(error.i)
		glewIsSupported(*param_)
	
	IncludeFile "glew_func_addr.pbi"
  
	EndImport


	IncludeFile "glew_include.pbi"

	IncludeFile "glew_globals.pbi"

EndDeclareModule


Module GLEW

IncludeFile "glew_proto_func.pbi"

IncludeFile "glew_func_calls.pbi"

Procedure glewInit()
	Define error = libGlewInit()
	If error = 0
	  IncludeFile "glew_init.pbi"
	Else
	  Debug PeekS( glewGetErrorString(error), -1, #PB_Ascii)
	EndIf
EndProcedure

EndModule
; IDE Options = PureBasic 5.31 (Windows - x64)
; CursorPosition = 3
; Folding = --
; EnableUnicode
; EnableXP
; CompileSourceDirectory