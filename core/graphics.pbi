﻿

DeclareModule SoaGraphics
	
	#WINDOW_Screen2DRequester = 0
	
	;---------------------------
	; Structures
	;---------------------------
	Structure GLTexture
		id.i
		width.i
		height.i
		depth.i
		inverted.b
		*bytes ;data
	EndStructure	
	
	Structure SoaSprite
		texture.GLTexture
		posX.i
		posY.i
		rotation.f
		accelX.f
		accelY.f
		velocityX.f
		velocityY.f
		scaleX.f
		scaleY.f
	EndStructure
	
	; Structure holding information about a graphic option
	Structure GRAPHIC_OPTIONS
		id.i
		Width.i
		Height.i
		Depth.i
		FullScreen.b
	EndStructure
	
	
	; Variable holding the FPS Timer
	Define FPSTimer.f
	
	; Variable holding the Frames per second
	Define FPS.i
	
	; How many frames has passed
	Define FPSCounter.i
	
	; Selected graphic option
	Global CurrentGraphicOption.GRAPHIC_OPTIONS

	; Procedure used to load OpenGL Textures
	Declare LoadGLTexture(*tex.GLTexture)	
	Declare ResetFPS()
	Declare GetFPS()
	Declare InitGraphicsSystem(*gOpt.GRAPHIC_OPTIONS, flags.i = #PB_Window_SystemMenu | #PB_Window_ScreenCentered, vSyng.i=#PB_Screen_NoSynchronization)
EndDeclareModule



Module SoaGraphics
	XIncludeFile "../lib/glew/glew.pbi"
	
	UseModule GLEW
	
	Procedure LoadGlTexture(*tex.GLTexture)
		
		UsePNGImageDecoder()
		
		Protected image = LoadImage(#PB_Any, "data/gonghead.png")
		Protected textureId.i
		
		glEnable(#GL_BLEND)
		glGenTextures(1, @textureId)
		glBindTexture(#GL_TEXTURE_2D, textureId)
		
		; grab the pixel data into the buffer 
		StartDrawing(ImageOutput(image))
		Protected *buffer = DrawingBuffer()
		Protected widthInBytes = DrawingBufferPitch()
		Protected dbpixelFormat.b = DrawingBufferPixelFormat()
		StopDrawing()
		
		Protected invertedY.b = dbpixelFormat & #PB_PixelFormat_ReversedY   
		Protected depth.b = dbpixelFormat ! #PB_PixelFormat_ReversedY
		Protected imgWidth.i = ImageWidth(image)
		Protected imgHeight.i = ImageHeight(image)
		
		
		
		Protected openGLDepth
		Select depth
			Case #PB_PixelFormat_8Bits   
				openGLDepth = #GL_RED
			Case #PB_PixelFormat_16Bits      
				openGLDepth = #GL_RG
			Case #PB_PixelFormat_24Bits_RGB  
				openGLDepth = #GL_RGB
			Case #PB_PixelFormat_24Bits_BGR 
				openGLDepth = #GL_BGR
			Case #PB_PixelFormat_32Bits_RGB  
				openGLDepth = #GL_RGBA
			Case #PB_PixelFormat_32Bits_BGR 
				
				openGLDepth = #GL_BGRA
		EndSelect
		
		glTexParameteri (#GL_TEXTURE_2D, #GL_TEXTURE_MIN_FILTER, #GL_LINEAR);
		glTexParameteri (#GL_TEXTURE_2D, #GL_TEXTURE_MAG_FILTER, #GL_NEAREST);
		glTexParameteri (#GL_TEXTURE_2D, #GL_TEXTURE_WRAP_S, #GL_CLAMP_TO_EDGE);
		glTexParameteri (#GL_TEXTURE_2D, #GL_TEXTURE_WRAP_T, #GL_CLAMP_TO_EDGE);
		
		glTexImage2D (#GL_TEXTURE_2D, 0, #GL_RGBA, imgWidth, imgHeight, 0, openGLDepth, #GL_UNSIGNED_BYTE, *buffer);
		
		glDisable(#GL_BLEND)
		
		*tex\id = textureId
		*tex\bytes = *buffer
		*tex\width = imgWidth
		*tex\height = imgHeight
		If  invertedY.b = #PB_PixelFormat_ReversedY  
			*tex\inverted = #True
		Else
			*tex\inverted = #False
		EndIf	
		FreeImage(image)
		
		
	EndProcedure
	
	; Reset FPS counter
	Procedure ResetFPS()
		Shared FPS
		Shared FPSTimer
		Shared FPSCounter
		FPS = 0
		FPSTimer = ElapsedMilliseconds()
		FPSCounter = 0
	EndProcedure
	
	
	; Get number of frames per second
	Procedure.i GetFPS()
		Shared FPS
		Shared FPSTimer
		Shared FPSCounter
		
		If ElapsedMilliseconds() > FPSTimer + 1000
			FPS = FPSCounter
			FPSCounter = 0
			FPSTimer = ElapsedMilliseconds()
		Else
			FPSCounter = FPSCounter +1
		EndIf
		
		ProcedureReturn FPS
	EndProcedure
	
	Procedure InitGraphicsSystem(*gOpt.GRAPHIC_OPTIONS, flags.i = #PB_Window_SystemMenu | #PB_Window_ScreenCentered, vSyng.i=#PB_Screen_NoSynchronization)
		Shared FPS
		Shared FPSTimer
		Shared FPSCounter
		
		; Initialize the Sprite system
		If InitSprite()=0                                                                                   
			MessageRequester("Error Initializing Graphics System","The Sprite system could not be initialized",0)
			End
		EndIf
		If InitKeyboard()=0                                                                                 
			MessageRequester("Error Initializing Graphics System","The keyboard system could not be initialized",0)
			End
		EndIf
		
		; If Fullscreen mode requested
		If *gOpt\FullScreen = #True
			Debug "FullScreen enabled"
			; If Windowed mode requested
		Else
			OpenWindow(#WINDOW_Screen2DRequester, 0, 0, *gOpt\Width, *gOpt\Height, "PureBasic Application", flags )
			
			If OpenWindowedScreen(WindowID(#WINDOW_Screen2DRequester), 0, 0, *gOpt\Width, *gOpt\Height, #False, 0, 0, vSyng)=0                                                                     
				MessageRequester("Error Openening Windowed Screen","An error has occured while opening the rendering screen",0)
				End
			EndIf
		EndIf
		
		UsePNGImageDecoder() 
		
		FPSTimer = ElapsedMilliseconds()
		FPS = 0
		FPSCounter = 0
	EndProcedure
EndModule
; IDE Options = PureBasic 5.31 (Windows - x64)
; CursorPosition = 33
; FirstLine = 21
; Folding = j-
; EnableUnicode
; EnableXP